const express = require('express')
const app = express()
const balance = require("./crypto-balance");
const transactions = require("./crypto-transactions");

app.get('/favicon.ico', (_, res) => res.send("") );

app.get('/:addr/transactions', (req, res) => {
    transactions(req.params.addr, req.query.type)
    .then(items => res.send(items))
    .catch(error => res.send({ "error": error.message }));
});

app.get('/:addr', (req, res) => {
    balance(req.params.addr, req.query.type)
    .then(items => res.send(items))
    .catch(error => res.send({ "error": error.message }));
});

app.get('/:coin/:addr', (req, res) => {
    balance(req.params.addr, req.params.coin)
    .then(items => res.send(items))
    .catch(error => res.send({ "error": error.message }));
});

app.get('/:coin/:addr/transactions', (req, res) => {
    transactions(req.params.addr, req.params.coin)
    .then(items => res.send(items))
    .catch(error => res.send({ "error": error.message }));
});

app.use('*', (req, res) => res.send({ "error": "invalid route" }) );

app.listen(8888, () => console.log('App listening on port 8888!') );