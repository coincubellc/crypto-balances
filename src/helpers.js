module.exports = {
    transactionType: {
        withdrawal: 'withdrawal',
        deposit: 'deposit',
    },

    transactionStatus: {
        ok: 'ok',
        failed: 'failed',
        pending: 'pending',
    },

    balanceFormatter(asset, quantity) {
        return {
            asset, // a common unified currency code, string
            quantity, // float
        };
    },

    transactionFormatter(asset, txid, timestamp, otherAddress, type, quantity, status, fee) {
        return {
            asset, // a common unified currency code, string
            txid, // string
            timestamp, // timestamp in milliseconds
            other_address: otherAddress, // "from" or "to", string
            type, // one of this.transactionType
            quantity, // float
            status, // one of this.transactionStatus
            fee // float
        };
    }
};
