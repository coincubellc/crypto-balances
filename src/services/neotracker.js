const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));

module.exports = {
    supported_address: [ "NEO" ],

    check(addr) {
        return RegExp('^A[0-9a-zA-Z]{33}$').test(addr);
    },
    
    symbol() {
        return "NEO";
    },

    balances(addr) {
        const url = `https://neoscan.io/api/main_net/v1/get_balance/${addr}`;

        return req(url, {json: true})
        .timeout(5000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            let amount = null;
            json.balance.forEach((balance) => {
                if (balance.asset === 'NEO') {
                    amount = balance.amount;
                }
            })
            return {
                quantity: amount,
                asset: 'NEO'
            };
        });
    },

    // transactions require continuous calls to https://neoscan.io/api/main_net/v1/get_address_abstracts/{addr}/{page}
};
