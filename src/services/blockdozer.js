const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

module.exports = {
    supported_address: [ "BCH" ],

    check(addr) {
        const regex1 = '^([13][a-km-zA-HJ-NP-Z1-9]{25,34})';
        const regex2 = '^((bitcoincash:)?(q|p)[a-z0-9]{41})';
        const regex3 = '^((BITCOINCASH:)?(Q|P)[A-Z0-9]{41})$';
        return RegExp(regex1).test(addr) || RegExp(regex2).test(addr) || RegExp(regex3).test(addr);
    },
    
    symbol() {
        return "BCH";
    },

    balances(addr) {
        const url = `https://blockdozer.com/insight-api/addr/${addr}`;

        return req(url, {json: true})
        .timeout(5000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            
            return formatter.balanceFormatter("BTC", json.balance);
        });
    },

    // transactions require continuous calls to https://blockdozer.com/insight-api/txs?address=${addr}&pageNum=${page}
};