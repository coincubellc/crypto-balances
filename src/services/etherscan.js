const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

module.exports = {
    supported_address: [ "ETH" ],

    check(addr) {
        return RegExp('^(0x)?[0-9a-fA-F]{40}$').test(addr);
    },
    
    symbol() {
        return "ETH";
    },

    // erc20 balances in ethplorer.js

    transactions(addr) {
        const url = `http://api.etherscan.io/api?module=account&action=tokentx&address=${addr}&startblock=0&endblock=99999999&&sort=desc`;
        return req(url, {json: true})
        .timeout(20000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            return json.result.map(t => formatter.transactionFormatter(
                t.tokenSymbol,
                t.hash,
                t.timeStamp * 1000,
                t.from === addr ? t.to : t.from,
                t.from === addr ? formatter.transactionType.withdrawal : formatter.transactionType.deposit,
                t.value / Math.pow(10, parseInt(t.tokenDecimal) || 0),
                parseInt(t.confirmations) > 0 ? formatter.transactionStatus.ok : formatter.transactionStatus.pending,
                t.gasUsed / Math.pow(10, parseInt(t.tokenDecimal) || 0)
            ))
        });
    },
}