const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

const conversion = 1000000000000000000;

module.exports = {
    supported_address: [ "ETH" ],

    check(addr) {
        return RegExp('^(0x)?[0-9a-fA-F]{40}$').test(addr);
    },
    
    symbol() {
        return "ETH";
    },

    balances(addr) {
        const url = `http://api.etherscan.io/api?module=account&action=balance&address=${addr}&tag=latest`;

        return req(url, {json: true})
        .timeout(5000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            return formatter.balanceFormatter("ETH", json.result / conversion);
        });
    },

    transactions(addr) {
        const url = `http://api.etherscan.io/api?module=account&action=txlist&address=${addr}&startblock=0&endblock=99999999&&sort=desc`;

        return req(url, {json: true})
        .timeout(20000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            return json.result.map(t => formatter.transactionFormatter(
                "ETH",
                t.hash,
                t.timeStamp * 1000,
                t.from === addr ? t.to : t.from,
                t.from === addr ? formatter.transactionType.withdrawal : formatter.transactionType.deposit,
                t.value / conversion,
                t.txreceipt_status === "1" ? formatter.transactionStatus.ok
                    : t.txreceipt_status === "0" ? formatter.transactionStatus.failed
                    : formatter.transactionStatus.pending,
                t.gasUsed / conversion
            ))
        });
    },
}