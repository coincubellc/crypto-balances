const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

module.exports = {
    supported_address: [ "LTC", "STRAT", "DGB" ],

    check(addr) {
        return RegExp('^[SMD][a-km-zA-HJ-NP-Z0-9]{26,33}$').test(addr);
    },

    symbol(addr) {
        return ({
            M: "LTC",
            S: "STRAT",
            D: "DGB",
        })[addr[0]];
    },

    balances(addr) {
        const network = this.symbol(addr);

        const url = `https://chainz.cryptoid.info/${network.toLowerCase()}/api.dws?q=getbalance&a=${addr}`;

        return req(url)
        .timeout(5000)
        .cancellable()
        .spread((resp, body) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            return formatter.balanceFormatter(network, parseFloat(body));
        });
    },

    // Only last 100 transactions

    transactions(addr) {
        const network = this.symbol(addr);

        const url = `https://chainz.cryptoid.info/explorer/address.summary.dws?coin=${network.toLowerCase()}&id=${addr}&fmt.js`;

        return req(url, {json: true})
        .timeout(20000)
        .cancellable()
        .spread((resp, body) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            if (!body.tx) throw new Error(JSON.stringify(body));
            let timestamp = 0;
            return body.tx.map(t => {
                timestamp += t[3] * 1000;
                return formatter.transactionFormatter(
                    network,
                    t[1],
                    timestamp,
                    "", // to/from address unavailable
                    t[4] < 0 ? formatter.transactionType.withdrawal : formatter.transactionType.deposit,
                    Math.abs(t[4]),
                    t[2] !== 0 ? formatter.transactionStatus.ok : formatter.transactionStatus.pending,
                    null
                )
            });
        });
    },
};