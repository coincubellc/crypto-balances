const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

module.exports = {
    supported_address: [ "BTC", "LTC", "DASH", "DOGE" ],

    check(addr) {
        return RegExp('^[LXD13][a-km-zA-HJ-NP-Z0-9]{26,33}$').test(addr);
    },

    symbol(addr) {
        return ({
            1: "BTC",
            3: "BTC",
            L: "LTC",
            X: "DASH",
            D: "DOGE"
        })[addr[0]];
    },

    balances(addr) {
        const network = this.symbol(addr);

        const url = `https://chain.so/api/v2/get_address_balance/${network}/${addr}`;

        return req(url, {json: true})
        .timeout(5000)
        .cancellable()
        .spread((resp, json) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            return formatter.balanceFormatter(network, parseFloat(json.data.confirmed_balance));
        });
    },

    // chain.so limited to 50 transactions, look into their other paginated requests and spends api

    // transactions(addr) {
    //     const network = this.symbol(addr);

    //     const url = `https://chain.so/api/v2/address/${network}/${addr}`;

    //     return req(url, {json: true})
    //     .timeout(20000)
    //     .cancellable()
    //     .spread(function(resp, json) {
    //         if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
    //         return json.data.txs.map(t => {
    //             const isWithdrawal = !!t.outgoing;
    //             const sentValue = isWithdrawal 
    //                 ? parseFloat(t.outgoing.value) - t.outgoing.outputs.reduce(
    //                     (agg, output) => output.address === addr ? agg + parseFloat(output.value) : agg,
    //                     0
    //                 )
    //                 : parseFloat(t.incoming.value);
    //             return formatter.transactionFormatter(
    //                 network,
    //                 t.txid,
    //                 t.time * 1000,
    //                 isWithdrawal ? t.outgoing.outputs[0].address : t.incoming.inputs[0].address,
    //                 isWithdrawal ? formatter.transactionType.withdrawal : formatter.transactionType.deposit,
    //                 sentValue,
    //                 t.confirmations > 0 ? formatter.transactionStatus.ok : formatter.transactionStatus.pending,
    //                 null
    //             )
    //         })
    //     });
    // },
};