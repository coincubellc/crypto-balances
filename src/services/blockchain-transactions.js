const Bluebird = require("bluebird");
const req = Bluebird.promisify(require("request"));
const formatter = require("../helpers");

const conversion = 100000000;
const maxTransactions = 10000;

const sum = (lst, cb) => lst.reduce((agg, l) => agg + parseFloat(cb(l)) / conversion, 0);

module.exports = {
    supported_address: [ "BTC" ],

    check(addr) {
        return RegExp('^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$').test(addr);
    },
    
    symbol() {
        return "BTC";
    },

    transactions(addr) {
        const url = page => `https://blockchain.info/rawaddr/${addr}?offset=${page*50}`;

        return req(url(0), {json: true})
        .timeout(10000)
        .cancellable()
        .spread((resp, body) => {
            if (resp.statusCode < 200 || resp.statusCode >= 300) throw new Error(JSON.stringify(resp));
            
            const numPages = Math.ceil(Math.min(body.n_tx, maxTransactions) / 50);
            const pages = Array(numPages).fill(undefined);

            return Bluebird.map(pages, (_, i) =>
                req(url(i), {json: true})
                .timeout(10000)
                .cancellable()
                .spread((resp, body) => {
                    if (resp.statusCode < 200 || resp.statusCode >= 300) return { err: resp };
                    if (!body.txs) {
                        return { err: body };
                    }
                    return body;
                })
                .catch(e => ({ err: e.message }))
            , { concurrency: 10 });
        })
        .then(all => all.reduce((agg, body) => {
            if (body.err) {
                throw new Error(JSON.stringify(body.err));
            }
            return agg.concat(body.txs.map(t => {
                const isWithdrawal = !!t.inputs.find(i => i.prev_out.addr === addr);
                return formatter.transactionFormatter(
                    "BTC",
                    t.hash,
                    t.time,
                    isWithdrawal
                        ? t.out.map(o => o.addr).find(o => o !== addr)
                        : t.inputs.map(i => i.prev_out.addr).find(i => i !== addr),
                    isWithdrawal ? formatter.transactionType.withdrawal : formatter.transactionType.deposit,
                    isWithdrawal
                        ? sum(t.out.filter(o => o.addr !== addr), o => o.value)
                        : sum(t.out.filter(o => o.addr === addr), o => o.value),
                    formatter.transactionStatus.ok,
                    isWithdrawal
                        ? sum(t.inputs, o => o.prev_out.value) - sum(t.out, o => o.value)
                        : 0
                );
            }));
        }, []));
    }
};
