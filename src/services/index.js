module.exports = {
    ethereum: require('./ethereum'),
    etherscan: require('./etherscan'),
    // erc20: require('./erc20'),
    chainso: require('./chain-so'),
    ethplorer: require('./ethplorer'),
    // etcchain: require('./etcchain'),
    chainz: require('./chainz'),
    blockchain_xpub: require('./blockchain-xpub'),
    blockchain_transactions: require('./blockchain-transactions'),
    blockdozer: require('./blockdozer'),
    neo: require('./neotracker')
};