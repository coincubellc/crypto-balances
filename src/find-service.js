const Bluebird = require("bluebird");
const services = require('./services');

module.exports = (addr, coin, serviceMethod, shouldMerge = true) => {
    let address_type = coin || "";
    return Bluebird
    .settle((() => {
        const result = [];
        for (let s in services) {
            const service = services[s];
            const supported = !coin || service.supported_address.map(c => c.toLowerCase()).includes(coin.toLowerCase());
            if (supported && service.check(addr) && service[serviceMethod]) {
                result.push(
                    service[serviceMethod](addr).catch(e => {
                        console.log(e);
                        return [{ error: `${s}: ${e.message}` }];
                    })
                );
                if (!address_type) address_type = service.symbol(addr);
            }
        }
        if(result.length === 0) return [[{ error: `no matches found` }]];
        return result;
    })())
    .timeout(100000)
    .cancellable()
    .map(asset => asset.isFulfilled() && asset.value())
    .reduce((a, b) => a.concat(b), [])
    .then(items => {
        let obj = {
            address_type,
            [serviceMethod]: shouldMerge ? {} : [],
        };
        let error;
        items.forEach(item => {
            if (item.error) {
                error = item.error;
                return;
            }
            if (shouldMerge) {
                if (item.hasOwnProperty('quantity')) {
                    obj[serviceMethod][item.asset] = item.quantity;
                }
            } else {
                obj[serviceMethod].push(item);
            }
        });
        if (Object.keys(obj[serviceMethod]).length === 0 && error) {
            throw new Error(error);
        }
        if (!shouldMerge) {
            obj[serviceMethod] = obj[serviceMethod].sort((a, b) => b.timestamp - a.timestamp);
        }
        return obj;
    })
    .catch(e => {
        console.error(e);
        return {
            address_type: 'unknown',
            error: e.message
        }
    });
}
;
