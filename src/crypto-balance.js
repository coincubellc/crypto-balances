const findService = require('./find-service');

module.exports = (addr, coin) => findService(addr, coin, "balances");